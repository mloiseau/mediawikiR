mediawikir
=========

An R package to connect an R script to a [MediaWiki](https://www.mediawiki.org/wiki/Download) instance. This also provides a class to perform batch page creation.
